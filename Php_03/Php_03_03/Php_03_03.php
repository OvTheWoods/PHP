<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden getAllValuesFromAnArray skal kunne finde værdierne i et associativt array.
     * Brug den indbyggede metode array_keys().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Keys and values
     */
    
    class Event
    {
        function getAllValuesFromAnArray()
        {
            $event = array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
            );
            $keys = array_keys($event);
            echo $event["EventId"] . $event["EventName"] . $event["EventDescription"] . $event["EventDate"] . $event["Lat"] . $event["Long"] . $event["EventImage"];
        }
        
    }
    $event = new Event;
    $event->getAllValuesFromAnArray();
?>