<!doctype html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <!-- Reference til bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
    <?php include("include/basket.php"); 
    ?>
    <?php include("include/register.php"); 
    ?>
    <div class="container">
        <div class="row">
            <h3>Udregn og returner prisen for varen inklusiv moms.</h3>
            <p>Indtast prisen uden moms</p>
            <h4><?php echo returnPriceInclusiveVAT(80); ?></h4>
        </div>
        <hr>
        <div class="row">
            <h3>Udregn og returner prisen for varen inklusiv moms.</h3>
            <p>Indtast prisen uden moms</p>
            <h4><?php echo returnDeductedVAT(100); ?></h4>
        </div>        
        <hr>
        <div class="row">
            <h3>Udregn prisen eksklusiv moms</h3>
            <p>Indtast prisen inklusiv moms</p>
            <h4><?php echo returnPriceExcludingVAT(100); ?></h4>
        </div>
        <div class="row">
            <h3>Udregn prisen for varen, når momsen er opgivet</h3>
            <p>Indtast momsen</p>
            <h4><?php echo returnPriceFromVAT(20); ?></h4>
        </div>
        <div class="row">
            <h3>Udregn prisen eksklusiv moms</h3>
            <p>Indtast prisen inklusiv moms</p>
            <h4><?php echo returnPriceWithDiscount(100, 25); ?></h4>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h3>Tilføj produkt til kurv</h3>
            <h4>
                <?php
                    $b= new Basket;
                    $p = $b->addProductToBasket();
                    var_dump($p);
                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Fjern produkt fra kurv</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->removeProductFromBasket(1);
                    var_dump($p);

                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Total antal produkter i kurven</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->returnQuantityInBasket();
                    var_dump($p);

                ?>
            </h4>
        </div>
        <div class="row">
            <h3>Total prisen for varerene i kurven</h3>
            <h4>
                <?php
                    $b = new Basket;
                    $p = $b->returnTotalPriceInBasket();
                    var_dump($p);
                ?>
            </h4>
        </div>
    </div>
    </body>
</html>