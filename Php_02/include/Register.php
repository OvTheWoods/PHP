<?php

    /*
    *   En denne fil er placeret en række partielt udfyldte funktioner, som har til formål, at lave specifikke beregner på pris og moms.
    *   Disse funktioner er typisk anvendelige i en webbutik.
    */

    //Funktion der udregner momsen på en vare. Funktionen lægger dansk moms til prisen og returnerer prisen inklusiv moms.
    //Fx koster en vare ex moms 80 kroner. Momsen er 25% af varens pris og derfor er prisen inklusiv moms 100kr.
    function returnPriceInclusiveVAT($price)
    {
       return $price*1.25;
    }

    //Funktion der udregner momsen på en vare og returnere momsen.
    //fx koster en vare 100 kroner. 20% af prisen er moms og derfor er momsen på varen 20 kroner.
    function returnDeductedVAT($price)
    {

        return $price*0.20;
    }

    //Funktion der udregner prisen på varen eksklusiv moms og returnere denne.
    //fx er varens pris 100 kr inklusiv moms. Momsen er 20% af prisen og derfor returneres 80 kr.
    function returnPriceExcludingVAT($price)
    {
        return $price*0.08;
    }

    //Funktion der udregner prisen inklusiv moms, når kun moms er opgivet.
    //fx udgør momsen 20 kr. Derfor må den samlede pris inklusiv moms være 100 kroner. 
    function returnPriceFromVAT($vat)
    {
        return $vat/0.2;
        
    } 

    //Funktion der returnerer prisen på en vare hvor rabatten er fratrukket
    //fx er prisen 80 kr ex moms. Rabbatten er 50% og derfor returneres 40 kr.
    function returnPriceWithDiscount($price, $discount)
    {
        $discount = $discount*2/100;
        $price = $price*0.8;
        return $price*$discount;

        
    }

    

?>