<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
include ("data.php");
?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                <?php
                
                echo "Name: ". $firstname." ".$lastname."<br>";
                echo "Birthday:". $birthday."<br>";
                echo "Email: ". $email. "<br>";
                ?>
            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>
                <?php

                foreach($preferences as $value){echo "$value <br>";}                
                
                ?>

            </div>
            <div class="col-md-3">
                <h2>Portefølje!</h2>

                <?php
                
                foreach($jobs as $location => $description){

                echo "hos ".$location. " lavede jeg ". $description. "<br>";
                }
                
                ?>

            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>
                <?php
                
                echo $competencies[0]."<br>";
                echo $competencies[1][0].", ".$competencies[1][1].", ".$competencies[1][2]. "<br>";
                echo $competencies[2][0].", ".$competencies[2][1].", ".$competencies[2][2]. "<br>";
                
                 foreach($competencies as $comp){
                if(is_array($comp)){
                    foreach($comp as $c){
                        echo $c;
                    }else{

                    }
                    echo $comp;
                }
            }
?>
            </div>
        </div>
    </div>
<body>