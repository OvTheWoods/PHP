<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (streng)
    $firstName = "Arne";

    //tekststreng (string)
    $lastName = "Svenningsen";

    //nummerisk heltal (integer)
    $age = "23";

    //boolean (sandt/falsk)
    $inRelationship = false;

    //tekststreng
    $work = "Studerende";

    //tekststreng
    $workPlace = "Erhvervsakademi Dania";

    //array (en række)
    $hobbies = ["Frederik's Mor", "Tv Shows", "Gaming"];
    ?>